package com.demo.falabella.json;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class LocalesByComuna {

    private List<Locales> locales;

    public List<Locales> getLocales() {
        return locales;
    }

    public void setLocales(List<Locales> locales) {
        this.locales = locales;
    }

    @Override
    public String toString() {
        return "LocalesByComuna{" +
                "locales=" + locales +
                '}';
    }

}
