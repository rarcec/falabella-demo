package com.demo.falabella.pojo;

public class Response {

    private String nombre_local;
    private String dicreccion;
    private String telefino;
    private String lat;
    private String lng;

    public Response(String nombre_local, String dicreccion, String telefino, String lat, String lng) {
        this.nombre_local = nombre_local;
        this.dicreccion = dicreccion;
        this.telefino = telefino;
        this.lat = lat;
        this.lng = lng;
    }

    public String getNombre_local() {
        return nombre_local;
    }

    public void setNombre_local(String nombre_local) {
        this.nombre_local = nombre_local;
    }

    public String getDicreccion() {
        return dicreccion;
    }

    public void setDicreccion(String dicreccion) {
        this.dicreccion = dicreccion;
    }

    public String getTelefino() {
        return telefino;
    }

    public void setTelefino(String telefino) {
        this.telefino = telefino;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }
}
