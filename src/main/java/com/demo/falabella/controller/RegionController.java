package com.demo.falabella.controller;

import com.demo.falabella.pojo.Response;
import com.demo.falabella.service.RegionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
public class RegionController {

    private static final Logger LOG = LoggerFactory.getLogger(RegionController.class);

    @Autowired
    private RegionService regionService;

    /**
     *
     * @param nameComuna Parametro que se utiliza para Filtrar por Comuna
     * @return Objeto de tipo Lista que muestra la informacion de la comuna.
     */

    @RequestMapping(value="rest/getLocalesByComuna", method = RequestMethod.GET)
    public @ResponseBody
    List<Response> getCasoSernac(@RequestParam String nameComuna) {

        LOG.info("INICIO - [rest/getLocalesByComuna]");
        List<Response> locales = regionService.getLocalesByComuna(nameComuna);
        LOG.info("FIN - [rest/getLocalesByComuna]");
        return locales;

    }

}
