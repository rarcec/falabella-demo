package com.demo.falabella.service;

import com.demo.falabella.json.Locales;
import com.demo.falabella.pojo.Response;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
public class RegionServiceImpl implements RegionService {

    private static final Logger log = LoggerFactory.getLogger(RegionServiceImpl.class);
    private static final Gson gson = new GsonBuilder().create();

    @Value("${url_service}")
    private String url_service;

    @Autowired
    private RestTemplate restTemplate;

    /**
     *
     * @param nameComuna Parametro que se encarga de filtrar por comuna
     * @return Un objeto de tipo lista solo con las datos de la comuna.
     */

    public List<Response> getLocalesByComuna(String nameComuna){

        log.info("Request: " + nameComuna);
        log.info("URL: " + this.url_service);

        //TODO: Llamar a servicio Web

        MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
        converter.setSupportedMediaTypes(Collections.singletonList(MediaType.ALL));

        ResponseEntity<List<Locales>> responseEntity;
        responseEntity = this.restTemplate.exchange(this.url_service, HttpMethod.GET,null,  new ParameterizedTypeReference<List<Locales>>() {});

        List<Locales> data = responseEntity.getBody();

        log.info("Response Service: " + gson.toJson(data));
        List<Response> response = new ArrayList<>();

        if(data != null){
            for (Locales locales : data){
                // TODO: Filtrar por comuna
                if(locales.getComuna_nombre().equals(nameComuna)){
                    Response respuesta = new Response(locales.getLocal_nombre()
                                                    ,locales.getLocal_direccion()
                                                    ,locales.getLocal_telefono()
                                                    ,locales.getLocal_lat()
                                                    ,locales.getLocal_lng());
                    response.add(respuesta);
                }

            }
            log.info("Response Service: " + gson.toJson(response));
            return response;
        }
        log.info("Response Service: NULL");
        return null;
    }
}
