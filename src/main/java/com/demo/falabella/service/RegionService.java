package com.demo.falabella.service;

import com.demo.falabella.pojo.Response;

import java.util.List;

public interface RegionService {

    List<Response> getLocalesByComuna(String nameComuna);


}
