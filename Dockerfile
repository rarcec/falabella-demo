FROM openjdk:11.0.6-jre
COPY "/build/libs/falabella-0.0.1-SNAPSHOT.jar" "app.jar"
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "app.jar"]