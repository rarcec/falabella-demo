# Comencemos.

### Documentación
Para ejecutar el microservicio es necesario hacer con el archivo Makefile que tiene los comandos necesarios para levantar la app.

* make build = comando que compila la app y crea tag (Se debe ingresar como parametro la funcion)
Ejemplo : make build V=1.0.0
* make run = Ejecuta el comando para levantar el contenedor en Docker.

* make all =  ejecuta ambos comandos de manera secuencial.


### En caso de no funcionar con Makefile en sus laptops

* Solo tomar los comandos del archivo Makefile y ejecutarlos via terminal.

### Aplicacion

* La aplicación corre en la siguiente URL: http://localhost:8080/rest/getLocalesByComuna?nameComuna=${COMUNA}