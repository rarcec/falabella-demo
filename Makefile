#MakeFile para ejecutar secuencias de comandos.

build:
	docker build -t falabella-demo:$V . --no-cache
run:
	docker run --rm -id -p 8080:8080 falabella-demo:$V
all:
	build run